import React, { Component } from 'react';
import { Grid, Cell } from 'react-mdl';
import './landingpage.css'



class Landing extends Component {
  render() {
    return(
      <div style={{width: '100%', margin: 'auto'}}>
        <Grid className="landing-grid">
          <Cell col={12}>
            <img
              src="image/PAN ACRD.jpg"
              alt="avatar"
              className="avatar-img"
              />

            <div className="banner-text">
              <h1>React js developer</h1>

    

          <p>HTML/CSS | Bootstrap | JavaScript | React JS | NodeJS | Express | MongoDB</p>

       
            </div>
          </Cell>
          
          <div className="banner-text">
          <ul className="resume-right-col">
            <li>	Experienced in application development using cutting- Edge Front-End Technologies like HTML5, CSS3, JAVASCRIPT, NPM,BOOTSTRAP,REACT, REDUX, JSON etc.</li>
            <li> 	Experienced worked with React-Redux architecture using ES6 features</li>
            <li>	Experience in developing Restful Web Services </li>
            <li>	Experienced in working with SQL Queries for fetching data from Tables and using Restful Webservices returning data to application in JSON Format.</li>
          </ul>
          </div>
        </Grid>
      </div>
      
    )
  }
}

export default Landing;
