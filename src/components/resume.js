import React, { Component } from 'react';
import { Grid, Cell } from 'react-mdl';
import Education from './education';
import Experience from './experience';
import Skills from './skills';


class Resume extends Component {
  render() {
    return(
      <div>
        <Grid>
          <Cell col={4}>
            <div style={{textAlign: 'center'}}>
              <img
                src="image/PAN ACRD.jpg"
                alt="avatar"
                className="avatar-img"
                 />
            </div>

            <h2 style={{paddingTop: '2em'}}>Sahaj Manandhar</h2>
            <h4 style={{color: 'grey'}}>Front end developer</h4>
            
            <p></p>
            <hr style={{borderTop: '3px solid #833fb2', width: '50%'}}/>
            <h5>Address</h5>
            <p>Kalimati, Kathmandu</p>
            <h5>Phone</h5>
            <p>9849967365</p>
            <h5>Email</h5>
            <p>sahajmdr@gmail.com</p>
            
          </Cell>
          <Cell className="resume-right-col" col={8}>
            <h2>Education</h2>


            <Education
              startYear={2010}
              endYear={2012}
              schoolName="Kathmandu Model college"
              
               />

               <Education
                 startYear={2014}
                 endYear={2017}
                 schoolName="KUSOM"
                 
                  />
                <hr style={{borderTop: '3px solid #e22947'}} />

              <h2>Experience</h2>

            <Experience
              startYear={2017}
              endYear={2020}
              jobName="Company Name: Alpha beta Theta"
              jobDescription=""
              />

              
              <hr style={{borderTop: '3px solid #e22947'}} />
              <h2>Skills</h2>
              <Skills
                skill="React"
                progress={100}
                />
                <Skills
                  skill="HTML/CSS"
                  progress={80}
                  />
                  <Skills
                    skill="Javascript"
                    progress={50}
                    />
                    <Skills
                      skill="Node js"
                      progress={40}
                      />


          </Cell>
        </Grid>
      </div>
    )
  }
}

export default Resume;
